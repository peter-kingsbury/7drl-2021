extends Node


onready var planet = $Setup/PlanetMesh
onready var options_dialog = $GUI/OptionsDialog
onready var fader = $Fader
onready var sfx = $SFX

export var planet_rotation_speed:float = 1.0
var sfx_timeout = 0.2


func _ready():
	pass


func _process(delta):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		#OS.set_window_fullscreen(!OS.window_fullscreen)
		Globals.toggle_fullscreen()
	
	planet.rotate_y(delta * planet_rotation_speed)


func reset_game():
	Globals.player_reset_all()
	pass


func _on_IntroductionButton_pressed():
	sfx.play("MouseClick")
	yield(get_tree().create_timer(sfx_timeout), "timeout")
	get_tree().change_scene("res://Scenes/Introduction/Introduction.tscn")


func _on_StartButton_pressed():
	sfx.play("MouseClick")
	yield(get_tree().create_timer(sfx_timeout), "timeout")
	Globals.start_new_game()
	reset_game()
	get_tree().change_scene("res://Levels/Habitat/HabitatLevel.tscn")


func _on_CreditsButton_pressed():
	sfx.play("MouseClick")
	yield(get_tree().create_timer(sfx_timeout), "timeout")
	get_tree().change_scene("res://Scenes/Credits/Credits.tscn")


func _on_ExitButton_pressed():
	sfx.play("MouseClick")
	yield(get_tree().create_timer(sfx_timeout), "timeout")
	get_tree().quit()


func _on_OptionsButton_pressed():
	sfx.play("MouseClick")
	yield(get_tree().create_timer(sfx_timeout), "timeout")
	options_dialog.show()


func _global_tick_timeout():
	Globals.log("Game::global_tick")


func _enter_tree():
#	fader.fade_in()
	pass


func _exit_tree():
#	fader.fade_out()
	pass


func _on_mouse_entered():
	sfx.play("MouseOver")
