extends Node

const fullscreen_toggle_delay_seconds = 0.5

var StartTime = 0

var ScreenSize:Vector2 = Vector2(800, 600)

var TileSize:float = 64
var TileSizev:Vector2 = Vector2(TileSize, TileSize)
var HalfTileSizev:Vector2 = TileSizev / 2

var Groups:Dictionary = {
	"Player": "player",
	"Enemy": "enemy"
}

var rng = RandomNumberGenerator.new()

var PixelMove:float = float(1000) / float(TileSize)

var Options = {
	"enable_crt_shader": true
}

enum Difficulty {
	EASY,
	NORMAL,
	HARD
}

var DifficultySettings = {
	"Easy": {
		"TickTimerSeconds": 15.0,
		"CraterCount": 2,
		"CrystalsPerCrater": 15,
		"EnemiesPerCrater": 5,
		"EnemyNoticeRange": 5,
		"EnemyDetectRadius": 288,
		"EnemyDamagePerHit": 1,
		"EnemyChaseOnLostQuarry": false,
		"BatteryPerStep": 0.1,
		"BatteryRechargeRate": 5,
		"HealthPerHit": 1,
		"InventoryCapacity": 999,
		"MiningLaserTimeoutSeconds": 1.5,
		"CrystalsGoal": 0.5
	},
	"Normal": {
		"TickTimerSeconds": 10.0,
		"CraterCount": 3,
		"CrystalsPerCrater": 10,
		"EnemiesPerCrater": 10,
		"EnemyNoticeRange": 4,
		"EnemyDetectRadius": 320,
		"EnemyChaseOnLostQuarry": false,
		"EnemyDamagePerHit": 3,
		"BatteryPerStep": 0.2,
		"BatteryRechargeRate": 4,
		"HealthPerHit": 1.5,
		"InventoryCapacity": 999,
		"MiningLaserTimeoutSeconds": 1.0,
		"CrystalsGoal": 0.6
	},
	"Hard": {
		"TickTimerSeconds": 5.0,
		"CraterCount": 4,
		"CrystalsPerCrater": 5,
		"EnemiesPerCrater": 15,
		"EnemyNoticeRange": 10,
		"EnemyDetectRadius": 352,
		"EnemyChaseOnLostQuarry": true,
		"EnemyDamagePerHit": 5,
		"BatteryPerStep": 0.3,
		"BatteryRechargeRate": 2,
		"HealthPerHit": 2.0,
		"InventoryCapacity": 999,
		"MiningLaserTimeoutSeconds": 0.8,
		"CrystalsGoal": 0.7
	}
}
var DifficultyLevel:int = Difficulty.NORMAL
var TickTimer:Timer = null
var TickTimerSeconds:float = 10.0

enum TerrainType { 
	NONE, 
	OVERLAND, 
	SUBTERRANEAN 
}

enum ConsoleTextType {
	NORMAL,
	
	DIRECTIVE,   			# directive("$msg"), blue
	RESPONSE_NORMAL,		# > $msg, white
	RESPONSE_DEBUG,			# > $msg, grey
	RESPONSE_OK, 			# > $msg, green
	RESPONSE_TIMEOUT,		# > $msg, orange
	RESPONSE_FAIL,			# > $msg, red
}

const TileSetFileExtension = "map"

var Player = {
	"inventory": [],
	"attributes": {
		"health": 100,
		"battery": 100,
	},
	"last_portal_id": -1
}

var GameCache = {
	"Levels": {
		"HabitatLevel": {},
		"OverlandLevel": {},
		"CraterLevel": {}
	}
}

var ConsoleText = []


func console_add_text(text:String, type:int, animate = true):
	var line = {
		"text": text,
		"type": type,
		"animate": animate
	}
	get_tree().call_group("robot_console", "add_text", text, type, animate)


func restore_console_text():
	for i in range(0, ConsoleText.size()):
		var line = ConsoleText[i]
		console_add_text(line.text, line.type, false)


func console_clear():
	ConsoleText = []
	get_tree().call_group("robot_console", "clear")


func _ready():
	StartTime = OS.get_unix_time()
#	self.log("randomize")
	rng.randomize()
#	self.log("initialize_tick_timer()")
	initialize_tick_timer()
	
	set_difficulty(DifficultyLevel)


func initialize_tick_timer():
	TickTimer = Timer.new()
	add_child(TickTimer)
	TickTimer.connect("timeout", self, "_on_tick_timer_timeout")
	TickTimer.set_wait_time(TickTimerSeconds)
	TickTimer.one_shot = false
	TickTimer.autostart = false
	TickTimer.start()


func _on_tick_timer_timeout():
	get_tree().call_group("global_tick", "_global_tick_timeout")


func reset_global_tick():
	TickTimer.stop()
	get_tree().call_group("global_tick", "_global_tick_timeout")
	TickTimer.set_wait_time(TickTimerSeconds)
	TickTimer.start()
	var settings = get_settings_by_difficulty()
	player_deplete_battery(settings.BatteryPerStep)
	player_heal(0)


func set_difficulty(difficulty:int):
	if difficulty == Difficulty.EASY:
		TickTimerSeconds = DifficultySettings.Easy.TickTimerSeconds
		reset_global_tick()
		
	elif difficulty == Difficulty.NORMAL:
		TickTimerSeconds = DifficultySettings.Normal.TickTimerSeconds
		reset_global_tick()
		
	elif difficulty == Difficulty.HARD:
		TickTimerSeconds = DifficultySettings.Hard.TickTimerSeconds
		reset_global_tick()
		
	else:
		self.log("Globals::set_difficulty(): Invalid difficulty level")


func log(text):
	var time = OS.get_unix_time()
	var diff = time - StartTime
	print("%08.1f: %s" % [ diff, text ])



func save_to_file(child:Node, filename:String) -> void:
	var file = File.new()
	var fn = "user://%s" % [ filename ]
	file.open(fn, File.WRITE)
	file.store_var(child, true)
	file.close()


func load_from_file(filename:String) -> Node:
	var file:File = File.new()
	
	var fn = "user://%s" % [ filename ]
	
	if not file.file_exists(fn):
		return null
	
	file.open(fn, File.READ)
	var o = file.get_var(true)
	file.close()
	return o


func get_settings_by_difficulty() -> Dictionary:
	match DifficultyLevel:
		Difficulty.EASY:
			return DifficultySettings.Easy
		Difficulty.NORMAL:
			return DifficultySettings.Normal
		Difficulty.HARD:
			return DifficultySettings.Hard
	return {}


func player_update_attributes():
	get_tree().call_group("player", "set_health", Player.attributes.health)
	get_tree().call_group("player", "set_battery", Player.attributes.battery)


func player_reset_all():
	Player.attributes.health = 100
	Player.attributes.battery = 100
	Player.inventory = []
	get_tree().call_group("player", "set_health", Player.attributes.health)
	get_tree().call_group("player", "set_battery", Player.attributes.battery)


func player_apply_damage(amount:float = 1):
	Player.attributes.health = clamp(Player.attributes.health - amount, 0, 100)
	get_tree().call_group("player", "set_health", Player.attributes.health)


func player_heal(amount:float = 1):
	Player.attributes.health = clamp(Player.attributes.health + amount, 0, 100)
	get_tree().call_group("player", "set_health", Player.attributes.health)


func player_deplete_battery(amount:float = 1):
	Player.attributes.battery = clamp(Player.attributes.battery - amount, 0, 100)
	get_tree().call_group("player", "set_battery", Player.attributes.battery)


func player_recharge_battery(amount:float = 1):
	Player.attributes.battery = clamp(Player.attributes.battery + amount, 0, 100)
	get_tree().call_group("player", "set_battery", Player.attributes.battery)


#func generate_tilemap(terrain_type, generate, map_w, map_h, iterations, min_cave_size, tiles):
#	var Terrain = load("res://Scenes/Terrain/Terrain.tscn")
#	var terrain = Terrain.instance()
#
#	terrain.terrain_type = 1
#	terrain.generate = 0
#	terrain.map_w = 40
#	terrain.map_h = 30
#	terrain.iterations = 32000
#	terrain.min_cave_size = 60
#	terrain.Tiles = tiles
#	terrain.generate()
#
#	return terrain


func start_new_game():
	var settings = get_settings_by_difficulty()
	
	# Build an empty structure for the game cache
	GameCache = {
		# Habitat level
		"Habitat": {
			
		},
		
		# Overland level
		"Overland": {
			"Cells": null,
			"Craters": [
#				{
#					"position": Vector2.ZERO,
#					"index": 0
#				}
			],
			"Habitat": {}
		},
		
		# Crater levels
		"Craters": [
#			{
#				"Cells": null,
#				"Aliens": [
#					{
#						"position": Vector2.ZERO,
#						"state": 0,
#						"damage": 0
#					}
#				],
#				"Exits": [
#					{
#						"position": Vector2.ZERO
#					}
#				],
#				"FuelCrystals": [
#					{
#						"position": Vector2.ZERO,
#						"damage": 0
#					}
#				],
#				"CrystalShards": [
#					{
#						"position": Vector2.ZERO
#					}
#				]
#			}
		]
	}
	return
	
func toggle_fullscreen():
	OS.set_window_fullscreen(!OS.window_fullscreen)
	yield(get_tree().create_timer(fullscreen_toggle_delay_seconds), "timeout")
	get_tree().call_group("crt_shader", "resize")

