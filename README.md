# 7drl-2021

Downloads:
* https://stormwarestudios.itch.io/7drl-2021

Repository:
* http://peter-kingsbury.gitlab.io/7drl-2021

## Install Instructions

Visit the itch.io website, under the **Download** section.

#### Windows

1. Save "7drl-2021-windows.zip" under Downloads to your computer.
1. Uncompress the archive.
1. Run RustyRobot.exe

#### Linux

1. Save "7drl-2021-linux.zip" under Downloads to your computer.
1. Uncompress the archive.
1. Run 7drl-2021.x86_64

#### macOS

1. Save "7drl-2021.zip" under Downloads to your computer.
1. Double-click to uncompress the archive.
1. Right-click RustyRobot (or RustyRobot.app) and choose Open.
1. Control-click the app icon, then choose Open from the shortcut menu.
1. Click Open.

Refer to the [macOS User Guide](https://support.apple.com/en-ca/guide/mac-help/mh40616/mac) for further details.


