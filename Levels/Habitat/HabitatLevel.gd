extends "res://Scenes/TemplateLevel/TemplateLevel.gd"

onready var game_over = $GUI/Control/GameOverDialog

func _ready():
	if not player:
		player = $Player/Player
		player.set_emit_dust_on_move(false)

	check_goals()

func _global_tick_timeout():
	Globals.log("HabitatLevel tick timeout")
	pass


func check_goals():
	var goal = player.get_goal_count()
	var inv = player.get_fuel_count()
	if inv >= goal:
		Globals.console_add_text("transport_fuel_to_habitat_system", Globals.ConsoleTextType.DIRECTIVE, true)
		game_over.set_mode_game_over()
