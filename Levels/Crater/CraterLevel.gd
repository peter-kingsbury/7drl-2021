extends "res://Scenes/TemplateLevel/TemplateLevel.gd"

var CraterTerrainSceneFile = "res://Scenes/Terrain/CraterTerrain.tscn"
var Alien = load("res://Scenes/Alien/Alien.tscn")
var FuelCrystal = load("res://Scenes/FuelCrystal/FuelCrystal.tscn")

onready var darkness = $Darkness
onready var crater_exit = $Doors/CraterExit

const GROUND_TILE = 3


func _ready():
	init_terrain()
	place_player()
	place_exit()
	place_crystals_v2()
#	place_aliens()
#	terrain.print_map()
	place_aliens_v2()
	
	var portal_id = Globals.Player.last_portal_id
	Globals.GameCache.Craters[portal_id].IsLoaded = true
	#player.restore_inventory()
	


func get_threat_count():
	var threats = 0
	var children = npcs.get_children()
	for i in children:
		var child = children[i]
		if child.current_behaviour == 5:
			threats = threats + 1
	return threats


func get_fuel_count():
	pass



func init_terrain():
	var portal_id = Globals.Player.last_portal_id
	var tilemap = generate_tilemap_v2(CraterTerrainSceneFile)
	
	if Globals.GameCache.Craters[portal_id].Cells == null:
		print("Generating level %s (id=%d)" % [ name, portal_id ])
		Globals.GameCache.Craters[portal_id].Cells = save_cells(tilemap)
	else:
		restore_cells(tilemap, Globals.GameCache.Craters[portal_id].Cells)
		print("Restoring level %s (id=%d) from cache" % [ name, portal_id ])
	terrain = tilemap
	
	navigation.add_child(tilemap)


func place_crystals_v2():
	var settings = Globals.get_settings_by_difficulty()
	var clearance_radius = 4
	
	var portal_id = Globals.Player.last_portal_id
	var load_from_cache = Globals.GameCache.Craters[portal_id].IsLoaded
	var object_count = 0
	
	if load_from_cache:
		object_count = Globals.GameCache.Craters[portal_id].FuelCrystals.size()
	else:
		object_count = settings.CrystalsPerCrater
	
	for i in range(0, object_count):
		var loc = null
		if load_from_cache:
			loc = Globals.GameCache.Craters[portal_id].FuelCrystals[i].position
		else:
			loc = terrain.find_empty_cell(GROUND_TILE, clearance_radius)
			Globals.GameCache.Craters[portal_id].FuelCrystals.push_back({
				"position": loc
			})
	
		var crystal = FuelCrystal.instance()
		items.add_child(crystal)
		crystal.position = loc * Globals.TileSizev - Globals.HalfTileSizev
		
		used_locations.push_back(loc)
	pass


func place_player():
	var clearance_radius = 3
	var settings = Globals.get_settings_by_difficulty()
	var portal_id = Globals.Player.last_portal_id
	var load_from_cache = Globals.GameCache.Craters[portal_id].IsLoaded
	
	var loc = Globals.GameCache.Craters[portal_id].Exit
	if loc == null:
		# Set the player somewhere safe-ish
		loc = terrain.find_empty_cell_v2(GROUND_TILE, clearance_radius)
		Globals.GameCache.Craters[portal_id].Exit = loc

	player.position = (loc + Vector2.DOWN) * Globals.TileSizev - Globals.HalfTileSizev
	
	used_locations.push_back(loc + Vector2.DOWN)


func place_exit():
	crater_exit.position = player.position - Vector2(0, Globals.TileSize)


func place_aliens_v2():
	var settings = Globals.get_settings_by_difficulty()
	
	for i in range(0, settings.EnemiesPerCrater):
		var clearance_radius = 2
		
		var loc = terrain.find_empty_cell(GROUND_TILE, clearance_radius)
		if loc:
			var alien = Alien.instance()
			alien.position = loc * Globals.TileSizev - Globals.HalfTileSizev
			npcs.add_child(alien)
			print("Alien at %d,%d (%f,%f)" % [ loc.x, loc.y, alien.position.x, alien.position.y ])
	return

