extends "res://Scenes/TemplateLevel/TemplateLevel.gd"

var OverlandTerrainSceneFile = "res://Scenes/Terrain/OverlandTerrain.tscn"
var Habitat = load("res://Scenes/Habitat/Habitat.tscn")
var CraterEntrance = load("res://Scenes/CraterEntrance/CraterEntrance_v2.tscn")


func _ready():
	init_terrain()
	place_habitat()
	place_craters()
#	terrain.print_map()


func init_terrain():
#	var overland_tileset = {
#		GROUND = 2, 
#		TREE = 5, 
#		WATER = 4, 
#		ROOF = 8
#	}
#	var overland = generate_tilemap(1, 0, 80, 60, 32000, 40, overland_tileset)
	var overland = generate_tilemap_v2(OverlandTerrainSceneFile)
	if Globals.GameCache.Overland.Cells == null:
		print("")
		Globals.GameCache.Overland.Cells = save_cells(overland)
	else:
		restore_cells(overland, Globals.GameCache.Overland.Cells)
	terrain = overland
	navigation.add_child(terrain)


func place_habitat():
	# Create the habitat
	var habitat = Habitat.instance()
	var loc = null
	
	if Globals.GameCache.Overland.Habitat.has("position"):
		loc = Globals.GameCache.Overland.Habitat.position
	else:
		loc = terrain.find_empty_cell_v2(2, 1)
		Globals.GameCache.Overland.Habitat.position = loc
	
	used_locations.push_back(loc)

	# Find a suitable location for the habitat
	habitat.position = loc * Globals.TileSizev - Globals.HalfTileSizev
	portals.add_child(habitat)
	
	# Connect the 'on body entered' signal
	habitat.portal.connect("portal_body_entered", self, "on_portal_body_entered")
	
#	player.position = (loc + Vector2.DOWN) * Globals.TileSizev - Globals.HalfTileSizev
	place_player()


func place_player():
	var loc = Vector2.ZERO
	var last_portal_id = Globals.Player.last_portal_id
	var craters = Globals.GameCache.Overland.Craters
	
	# Player entered a crater most recently
	if last_portal_id >= 0 and last_portal_id < craters.size():
		loc = craters[last_portal_id].position
	
	# Set the player to just outside the habitat
	else:
		loc = Globals.GameCache.Overland.Habitat.position + Vector2.DOWN
	
	player.position = loc * Globals.TileSizev - Globals.HalfTileSizev


func find_crater_location():
	var iterations = 100000
	var loc
	var result = Vector2.ZERO
	for i in range(0, iterations):
		loc = terrain.find_empty_cell_v2(2, 2)
		print(loc)
		if used_locations.find(loc) == -1:
			used_locations.push_back(loc)
			result = loc
			break
#	print('failed to get crater location')
	return loc


func place_craters():
	var settings = Globals.get_settings_by_difficulty()
	var crater_count = settings.CraterCount

	var load_from_cache = false
	if Globals.GameCache.Overland.Craters.size() > 0:
		load_from_cache = true
	else:
		Globals.GameCache.Craters = []
	
	for i in range(0, crater_count):
		var crater = CraterEntrance.instance()
		var loc = null
		var portal_id = null
		
		if load_from_cache:
			loc = Globals.GameCache.Overland.Craters[i].position
			portal_id = Globals.GameCache.Overland.Craters[i].portal_id
		else:
			loc = find_crater_location()
			portal_id = i
			Globals.GameCache.Overland.Craters.append({
				"position": loc,
				"portal_id": portal_id
			})
	
		crater.position = loc * Globals.TileSizev - Globals.HalfTileSizev
		crater.portal_id = portal_id
		craters.add_child(crater)
		crater.connect("portal_body_entered", self, "on_portal_body_entered")
		
		# Create raw entry for crater level
		var crater_level = {
			"IsLoaded": false,
			"PortalId": portal_id,
			"Cells": null,
			"Aliens": [],
			"Exit": null,
			"FuelCrystals": [],
			"CrystalShards": []
		}
		Globals.GameCache.Craters.push_back(crater_level)


func recharge_player_battery():
	var settings = Globals.get_settings_by_difficulty()
	var amount = 0
	
	match Time.current_cycle:
		Time.CycleState.NIGHT:
			# Battery does not recharge at night; do nothing.
			return
		Time.CycleState.DAWN:
			amount = settings.BatteryRechargeRate / 2
		Time.CycleState.DAY:
			amount = settings.BatteryRechargeRate
		Time.CycleState.DUSK:
			amount = settings.BatteryRechargeRate / 2
	Globals.player_recharge_battery(amount)


func _global_tick_timeout():
	recharge_player_battery()
