tool
extends TileMap

#export(int)   var terrain_type  = 0
export (
	int, 
	"None", 
	"Overland", 
	"Subterranean"
)             var terrain_type   = 0
export(bool)  var generate		 = false
export(int)   var map_w          = 16
export(int)   var map_h          = 12
export(int)   var iterations     = 10000
export(int)   var neighbors      = 4
export(int)   var ground_chance  = 48
export(int)   var min_cave_size  = 80
export(int)   var flood_overfill = 32

export(bool)  var redraw  setget redraw

export var Tiles = {
	GROUND = 2, 
	TREE = 5, 
	WATER = 4, 
	ROOF = 8
}

var caves = []

func set_cell_override(x:int, y:int, tile:int):
	var flip_x: bool = false
	var flip_y: bool = false
	var transpose: bool = false
	var autotile_coord: Vector2 = Vector2( 0, 0 )
	return set_cell(x, y, tile, flip_x, flip_y, transpose, autotile_coord)


func set_tiles():
	match terrain_type:
		Globals.TerrainType.NONE:
			pass
		Globals.TerrainType.OVERLAND:
			pass
		Globals.TerrainType.SUBTERRANEAN:
			Tiles = {
				GROUND = 3, 
				TREE = 5, 
				WATER = 4, 
				ROOF = 9
			}
			pass
	pass


func _ready():
	set_tiles()
	
	if generate:
		generate()


func check_stuff(x, y, type, radius = 1):
	var start_x = x - radius
	var end_x = x + radius
	var start_y = y - radius
	var end_y = y + radius
	
	var i = start_x
	var j = start_y
	
	while j < end_y:
		while i < end_x:
			var found_tile = in_tile(get_cell(i, j), type)
			#print(i, ",", j, " ", type, " ", found_tile)
			if not found_tile:
				return false
			i = i + 1
			# end while i
		j = j + 1
		# end while j
	
	return true


func check_tile_radius(x, y, type, radius = 1) -> bool:
	for j in range(y - radius, y + radius):
		for i in range(x - radius, x + radius):
			if get_cell(i, j) != type:
				return false
	return true


func find_empty_cell(type, radius = 1):
	randomize()
	
	var cells = get_cells_of_type(type)
	cells.shuffle()
	cells.shuffle()
	cells.shuffle()
	
	for cell in cells:
		if check_tile_radius(cell.x, cell.y, type, radius):
			return cell
	
	return null


func find_empty_cell_v2(type, radius = 1):
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var result = Vector2.ZERO
	
	var iterations = 1000000
	
	for i in range(0, iterations):
		var x = rng.randi_range(radius, map_w - radius)
		var y = rng.randi_range(radius, map_h - radius)
		print("Checking %d,%d" % [x, y])
		if check_tile_radius(x, y, type, radius):
			result = Vector2(x, y)
			break
	
	return result


func redraw(value = null):
	# only do this if we are working in the editor
	if !Engine.is_editor_hint(): return
	
	if generate:
		generate()


func generate():
	clear()
	fill_roof()
	random_ground()
	dig_caves()
	get_caves()
	connect_caves()
	clear_floor_tiles()
	auto_tile()


func clear_floor_tiles():
	match terrain_type:
		Globals.TerrainType.NONE:
			pass
		Globals.TerrainType.OVERLAND:
			pass
		Globals.TerrainType.SUBTERRANEAN:
			for x in range(0, map_w):
				for y in range(0, map_h):
					var cell = get_cell(x, y)
					if cell == Tiles.ROOF:
						set_cell(x, y, -1)
					pass


func auto_tile_subterranean():
	for x in range(0, map_w):
		for y in range(0, map_h):
			var tile = get_cell(x, y)
			update_bitmask_area(Vector2(x, y))


func auto_tile():
#	print("auto_tile()")
	match terrain_type:
		Globals.TerrainType.NONE:
			print("none... pass")
			pass
		Globals.TerrainType.OVERLAND:
			print("overland... pass")
			pass
		Globals.TerrainType.SUBTERRANEAN:
			print("subterranean... processing")
			auto_tile_subterranean()


func pick_tile(type):
	return type 


func in_tile(tile, type):
	return tile == type


func swap_tiles():
	for x in range(0, map_w):
		for y in range(0, map_h):
			set_cell(x, y, get_cell(x, y))


# start by filling the map with roof tiles
func fill_roof():
	for x in range(0, map_w):
		for y in range(0, map_h):
			set_cell(x, y, pick_tile(Tiles.ROOF))


# then randomly place ground tiles
func random_ground():
	for x in range(1, map_w-1):
		for y in range(1, map_h-1):
			if Util.chance(ground_chance):
				set_cell(x, y, pick_tile(Tiles.GROUND))


func dig_caves():
	randomize()
	
	for i in range(iterations):
		# Pick a random point with a 1-tile buffer within the map
		var x = floor(rand_range(1, map_w-1))
		var y = floor(rand_range(1, map_h-1))
	
		# if nearby cells > neighbors, make it a roof tile
		if check_nearby(x,y) > neighbors:
			set_cell(x, y, pick_tile(Tiles.ROOF))
	
		# or make it the ground tile
		elif check_nearby(x,y) < neighbors:
			set_cell(x, y, pick_tile(Tiles.GROUND))


func get_caves():
	caves = []
	
	# locate all the caves and store them
	for x in range (0, map_w):
		for y in range (0, map_h):
			if in_tile(get_cell(x, y), Tiles.GROUND):
				flood_fill(x,y)
	
	for cave in caves:
		for tile in cave:
			set_cellv(tile, pick_tile(Tiles.GROUND))


func flood_fill(tilex, tiley):
	# flood fill the separate regions of the level, discard
	# the regions that are smaller than a minimum size, and
	# create a reference for the rest.
	
	var cave = []
	var to_fill = [Vector2(tilex, tiley)]
	while to_fill:
		var tile = to_fill.pop_back()
	
		if !cave.has(tile):
			cave.append(tile)
			set_cellv(tile, pick_tile(Tiles.ROOF))
	
			#check adjacent cells
			var north = Vector2(tile.x, tile.y-1)
			var south = Vector2(tile.x, tile.y+1)
			var east  = Vector2(tile.x+1, tile.y)
			var west  = Vector2(tile.x-1, tile.y)
	
			for dir in [north,south,east,west]:
				if get_cellv(dir) == Tiles.GROUND:
					if !to_fill.has(dir) and !cave.has(dir):
						to_fill.append(dir)
	
	if cave.size() >= min_cave_size:
		caves.append(cave)


func connect_caves():
	var prev_cave = null
	var tunnel_caves = caves.duplicate()
	
	for cave in tunnel_caves:
		if prev_cave:
			var new_point  = Util.choose(cave)
			var prev_point = Util.choose(prev_cave)
	
			# ensure not the same point
			if new_point != prev_point:
				create_tunnel(new_point, prev_point, cave)
	
		prev_cave = cave


# do a drunken walk from point1 to point2
func create_tunnel(point1, point2, cave):
	randomize()          # for randf
	var max_steps = 500  # set a max_steps so editor won't hang if walk fails
	var steps = 0
	var drunk_x = point2[0]
	var drunk_y = point2[1]
	
	while steps < max_steps and !cave.has(Vector2(drunk_x, drunk_y)):
		steps += 1
	
		# set initial dir weights
		var n       = 1.0
		var s       = 1.0
		var e       = 1.0
		var w       = 1.0
		var weight  = 1
	
		# weight the random walk against edges
		if drunk_x < point1.x: # drunkard is left of point1
			e += weight
		elif drunk_x > point1.x: # drunkard is right of point1
			w += weight
		if drunk_y < point1.y: # drunkard is above point1
			s += weight
		elif drunk_y > point1.y: # drunkard is below point1
			n += weight
	
		# normalize probabilities so they form a range from 0 to 1
		var total = n + s + e + w
		n /= total
		s /= total
		e /= total
		w /= total
	
		var dx
		var dy
	
		# choose the direction
		var choice = randf()
	
		if 0 <= choice and choice < n:
			dx = 0
			dy = -1
		elif n <= choice and choice < (n+s):
			dx = 0
			dy = 1
		elif (n+s) <= choice and choice < (n+s+e):
			dx = 1
			dy = 0
		else:
			dx = -1
			dy = 0
	
		# ensure not to walk past edge of map
		if (2 < drunk_x + dx and drunk_x + dx < map_w-2) and \
			(2 < drunk_y + dy and drunk_y + dy < map_h-2):
			drunk_x += dx
			drunk_y += dy
			if in_tile(get_cell(drunk_x, drunk_y), Tiles.ROOF):
				set_cell(drunk_x, drunk_y, pick_tile(Tiles.GROUND))
	
				# optional: make tunnel wider
				set_cell(drunk_x+1, drunk_y, pick_tile(Tiles.GROUND))
				set_cell(drunk_x+1, drunk_y+1, pick_tile(Tiles.GROUND))


# check in 8 dirs to see how many tiles are roofs
func check_nearby(x, y):
	var count = 0
	if in_tile(get_cell(x, y-1), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x, y+1), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x-1, y), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x+1, y), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x+1, y+1), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x+1, y-1), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x-1, y+1), Tiles.ROOF):  count += 1
	if in_tile(get_cell(x-1, y-1), Tiles.ROOF):  count += 1
	return count


func check_nearby_for_type(x, y, type):
	var count = 0
	if in_tile(get_cell(x, y-1), type):    count += 1
	if in_tile(get_cell(x, y+1), type):    count += 1
	if in_tile(get_cell(x-1, y), type):    count += 1
	if in_tile(get_cell(x+1, y), type):    count += 1
	if in_tile(get_cell(x+1, y+1), type):  count += 1
	if in_tile(get_cell(x+1, y-1), type):  count += 1
	if in_tile(get_cell(x-1, y+1), type):  count += 1
	if in_tile(get_cell(x-1, y-1), type):  count += 1
	return count


func get_cells_of_type(type):
	var all = get_used_cells()
	var cellsOfIndex = []
	for v in all:
		if get_cell(v.x, v.y) == type:
			cellsOfIndex.append(v)
	return cellsOfIndex

func print_map():
	print("Map: %s" % [ name ])
	for j in range(0, map_h):
		var line = ""
		for i in range(0, map_w):
			var cell = get_cell(i, j)
			if cell == -1:
				cell = " "
			line = line + str(cell) + " "
		print(line)

