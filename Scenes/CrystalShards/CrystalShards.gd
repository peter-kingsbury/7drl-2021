extends "res://Scenes/TemplateItem/TemplateItem.gd"

onready var light = $Light2D
onready var sfx = $SFX

export var light_fade_speed = 1.0
export var min_glow_level = 0.5

func _ready():
	var sounds = ["Glass1", "Glass2", "Glass3", "Glass4", "Glass5"]
	sounds.shuffle()
	sfx.play_random_sequence(sounds, 0.05)


func _process(delta:float) -> void:
	if light.energy > min_glow_level:
		light.energy -= light_fade_speed * delta


func _on_Area2D_body_entered(body):
	if not "is_player" in body:
		return

	# Player collided with crystal shards
#	print("Player picked up crystal shards")
	get_tree().call_group("player", "take", self)
	
