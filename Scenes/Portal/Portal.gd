extends Node2D

signal portal_body_entered
signal portal_body_exited

onready var sprite = $TemplateSprite
onready var marker = $Marker
onready var collision_area = $Area2D
onready var activation_timer = $ActivationTimer

export var target_scene:String = ""
export var portal_id:int = -1


# Called when the node enters the scene tree for the first time.
func _ready():
	marker.hide()

#	collision_area.monitoring = false
	activation_timer.start()


func _on_Area2D_body_entered(body):
	print("Collision with portal id %d" % [ portal_id ])
	Globals.Player.last_portal_id = portal_id
	emit_signal("portal_body_entered", target_scene, portal_id)
	if target_scene == "":
		pass
	else:
		print("loading scene %s ..." % [ target_scene ])
		get_tree().change_scene(target_scene)


func _on_Area2D_body_exited(body):
	emit_signal("portal_body_exited")


func activate():
#	print("%s is open" % [ name ])
	collision_area.monitoring = true


func _on_ActivationTimer_timeout():
	activate()
