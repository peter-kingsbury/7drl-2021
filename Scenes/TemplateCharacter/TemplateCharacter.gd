extends KinematicBody2D

var level = null
var tile_position:Vector2 = Vector2.ZERO
onready var SFX = $SFX


func _ready() -> void:
	pass


func _process(delta) -> void:
	update_tile_position()


func set_level(lvl):
	level = lvl


func get_level():
	return level


func update_tile_position():
	tile_position = (position / Globals.TileSize).floor()


func play_sound(name):
	var sfx:AudioStreamPlayer2D = SFX.find_node(name, true, false)
	if sfx:
		sfx.play()
