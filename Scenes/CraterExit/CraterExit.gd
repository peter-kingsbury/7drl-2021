extends Node2D

var can_enter = true
onready var entry_timer = $EntryTimer
onready var collision_area = $Area2D


func _ready():
	collision_area.monitoring = false
	pass


func _on_Area2D_body_entered(body):
	if not "is_player" in body:
		return
	
	# TODO: Move the player to overland
	if body.has_moved and can_enter:
		print("You exit the crater ...")
		get_tree().change_scene("res://Levels/Overland/OverlandLevel.tscn")
	else:
		print("can not enter yet")


func activate():
	collision_area.monitoring = true
