extends Node2D

export var enemy_count = 3

onready var enemies = $Enemies
onready var players = $Players
onready var timer = $Timer

var player = null

var Enemy = load("res://Scenes/Alien/Alien.tscn")
var Player = load("res://Scenes/Player/Player.tscn")

var player_can_act = false

func index_move(node, rel_x, rel_y):
	node.position.x += rel_x * Globals.TileSize
	node.position.y += rel_y * Globals.TileSize


func _ready():
	init_enemies()
	init_player()


func _input(event):
	if event.is_action_pressed("ui_accept"):
		index_move(player, 1, 1)
		pass
	pass


func init_enemies():
	var min_x = 2
	var min_y = 2
	var max_x = (Globals.ScreenSize.x / Globals.TileSize) - 2
	var max_y = (Globals.ScreenSize.y / Globals.TileSize) - 2
	
	for i in range(0, enemy_count):
		var enemy = Enemy.instance()
		var x = Globals.rng.randi_range(min_x, max_x)
		var y = Globals.rng.randi_range(min_y, max_y)
		enemies.add_child(enemy)
		enemy.position = Vector2(x * Globals.TileSize, y * Globals.TileSize)
		enemy.light.hide()


func init_player():
	var min_x = 1
	var min_y = 1
	var max_x = Globals.ScreenSize.x / Globals.TileSize
	var max_y = Globals.ScreenSize.y / Globals.TileSize
	var x = Globals.rng.randi_range(min_x, max_x)
	var y = Globals.rng.randi_range(min_y, max_y)
	var p = Player.instance()
	players.add_child(p)
	p.position = Vector2(x * Globals.TileSize, y * Globals.TileSize)
	player = p


func update_enemies():
	for child in enemies.get_children():
		update_enemy(child)



func update_enemy(enemy):
	var x = Globals.rng.randi_range(-1, 1)
	var y = Globals.rng.randi_range(-1, 1)
#	var rel_x = enemy.position.x + x * Globals.TileSize
#	var rel_y = enemy.position.y + y * Globals.TileSize
#	enemy.target_position = Vector2(rel_x, rel_y)
	index_move(enemy, x, y)


func _on_Timer_timeout():
	player_can_act = true
	update_enemies()
