extends "res://Scenes/TemplateCharacter/TemplateCharacter.gd"

onready var dust_particles = $Visuals/NormalMode/DustParticles
onready var scan_up = $Debug/ScanUp
onready var scan_down = $Debug/ScanDown
onready var scan_left = $Debug/ScanLeft
onready var scan_right = $Debug/ScanRight
onready var laser = $LaserBeam2D
onready var visuals = $Visuals
onready var laser_timer = $LaserTimer
onready var tile_position_label = $Debug/TilePositionLabel
onready var normal_mode = $Visuals/NormalMode
onready var low_battery_mode = $Visuals/LowBatteryMode
onready var goals_timer = $GoalsTimer
onready var sfx = $SFX

var is_moving:bool = false
var motion = Vector2()
var motion_target = Vector2()
var aim_target = Vector2()
var can_move:bool = true
var has_moved:bool = false

export var emit_dust_on_move:bool = true
export var speed:float = 256.0
var show_test = false
var alive = true
#var half_tile = Globals.HalfTileSizev

var up = Vector2(0, -1) * Globals.HalfTileSizev
var down = Vector2(0, 1) * Globals.HalfTileSizev
var left = Vector2(-1, 0) * Globals.HalfTileSizev
var right = Vector2(1, 0) * Globals.HalfTileSizev
#var low_battery_mode = false

var is_player = true


#onready var scan_vectors = {
#	"up": {
#		"direction": up,
#		"control": scan_up
#	},
#	"down": {
#		"direction": down,
#		"control": scan_down
#	},
#	"left": {
#		"direction": left,
#		"control": scan_left
#	},
#	"right": {
#		"direction": right,
#		"control": scan_right
#	},
#}

onready var headlights = $Visuals/NormalMode/Headlights

enum Mode {
	NORMAL,
	LOW_BATTERY
}

export (int, "Normal", "Low Battery") var mode setget set_mode,get_mode


func _ready():
	get_tree().call_group("enemy", "set_player_tile_position", position / Globals.TileSizev - Vector2(0.5, 0.5))
	init_laser_timer()
	laser.show()
	restore_inventory()
#	kernel_panic()
#	headlights.hide()
	reset()
	show_goals()


func reset():
	is_moving = false
	motion = Vector2()
	motion_target = Vector2()
	aim_target = Vector2()
	can_move = true
	has_moved = false



func set_mode(new_mode):
	match new_mode:
		0:
			print("resuming normal operation")
			normal_mode.show()
			low_battery_mode.hide()
		1:
			print("low battery")
			normal_mode.hide()
			low_battery_mode.show()
		_:
			print("Invalid mode %d" % [ new_mode ])
			return
	
	mode = new_mode


func get_mode():
	return mode


func init_laser_timer():
	var settings = Globals.get_settings_by_difficulty()
	laser_timer.wait_time = settings.MiningLaserTimeoutSeconds


func fire_laser():
	if get_mode() == Mode.LOW_BATTERY:
		return
	
	if not alive:
		return
	
	laser.set_is_casting(true)
	laser_timer.start()
	sfx.play("Laser")


func fire_laser_end():
	laser.set_is_casting(false)
	laser_timer.stop()
	Globals.reset_global_tick()
	sfx.stop("Laser")


func _process(delta) -> void:
	if Input.is_action_just_pressed("toggle_fullscreen"):
		#OS.set_window_fullscreen(!OS.window_fullscreen)
		Globals.toggle_fullscreen()
	
	if Input.is_action_just_pressed("fire_laser"):
		fire_laser()
	elif Input.is_action_just_released("fire_laser"):
		fire_laser_end()
	
	#tile_position_label.text = "(%d, %d)" % [ tile_position.x, tile_position.y ]
	
	get_tree().call_group("enemy", "set_player_tile_position", tile_position)


func scan_for_collision(vec) -> KinematicCollision2D:
	var direction = vec * Globals.TileSize / 2
	return move_and_collide(direction, false, true, true)


func check_can_move(direction) -> bool:
	return null == move_and_collide(direction, true, true, true)


func _input(event):
	if event.is_action_pressed("exit_to_main_menu") and not alive:
		get_tree().change_scene("res://Game.tscn")
	
	if Input.is_action_just_pressed("toggle_exit_dialog"):
		print("toggle quit dialog")
		get_tree().call_group("exit_dialog", "toggle_visibility")


func _physics_process(delta:float) -> void:
	snap_motion()
	snap_position()
	update_movement(delta)
#	update_collision_scan()
	aim_laser()
	move_and_slide(motion * speed)


func aim_laser():
	laser.rotation_degrees = rad2deg(aim_target.angle())


func snap_motion() -> void:
	if int(round(position.x)) == int(round(motion_target.x)):
		motion.x = 0
	if int(round(position.y)) == int(round(motion_target.y)):
		motion.y = 0


func snap_position() -> void:
	if motion == Vector2.ZERO:
		can_move = true
		position.x = int(round(position.x))
		position.y = int(round(position.y))


func update_movement(delta:float) -> void:
	if not can_move:
		return
	
	if get_mode() == Mode.LOW_BATTERY:
		return
	
	if not alive:
		return
	
	if Input.is_action_just_pressed("move_down") and not Input.is_action_pressed("move_up"):
		aim_target = Vector2(0, 1)
		if check_can_move(down):
			motion.y = 1
	elif Input.is_action_just_pressed("move_up") and not Input.is_action_pressed("move_down"):
		aim_target = Vector2(0, -1)
		if check_can_move(up):
			motion.y = -1
	else:
		motion.y = 0
	pass
	
	if Input.is_action_just_pressed("move_left") and not Input.is_action_pressed("move_right"):
		aim_target = Vector2(-1, 0)
		visuals.scale.x = -1
		if check_can_move(left):
			motion.x = -1
	elif Input.is_action_just_pressed("move_right") and not Input.is_action_pressed("move_left"):
		aim_target = Vector2(1, 0)
		visuals.scale.x = 1
		if check_can_move(right):
			motion.x = 1
	else:
		motion.x = 0
	
	motion_target = position + (motion * Globals.TileSize)
	
	if motion == Vector2.ZERO:
		position.x = int(round(position.x))
		position.y = int(round(position.y))
		can_move = true
		get_tree().call_group("portal", "activate")
		dust_particles.emitting = false
	else:
		get_tree().call_group("enemy", "set_player_tile_position", position / Globals.TileSizev - Vector2(0.5, 0.5))
		Globals.reset_global_tick()
		can_move = false
		show_test = true
		has_moved = true
		dust_particles.emitting = true


func set_emit_dust_on_move(dust:bool) -> void:
	emit_dust_on_move = dust


func inventory_has_room() -> bool:
	var settings = Globals.get_settings_by_difficulty()
	if Globals.Player.inventory.size() < settings.InventoryCapacity:
		return true
	return false


func push_inventory(item_type):
	Globals.Player.inventory.push_back(item_type)



func pop_inventory() -> int:
	return Globals.Player.inventory.pop_back()


func restore_inventory():
	for i in Globals.Player.inventory:
		get_tree().call_group("inventory", "add_item", i)
	pass


func take(item):
	var settings = Globals.get_settings_by_difficulty()
	match item.item_type:
		1: # fuel
			if inventory_has_room():
				push_inventory(item.item_type)
				item.queue_free()
				get_tree().call_group("inventory", "add_item", item.item_type)
				show_goals()
				check_end_goal()
				sfx.play("Take")

	return


func _on_LaserTimer_timeout():
	Globals.reset_global_tick()


func kernel_panic():
	var lines = [
		"Kernel bug detected[#1][cpu0]:",
		"$ 0   : 00000000 00000000 0fffff00 0fffff00",
		"$ 4   : c6800000 850b7780 8b0043c0 00000040",
		"$ 8   : 000004ac 000004ac 00000002 844e8000",
		"Stack : 8b0323d8 8402d074 843d0c68 843d10e8 ",
		"        8b2d6de8 8402d194 00000003 00000000 ",
		"Code: 3c030fff  3463ff00  00431024 <00028036> ",
		"Kernel panic - not syncing: Fatal exception in interrupt"
	]
	
	for line in lines:
		get_tree().call_group("robot_console", "add_text", line, Globals.ConsoleTextType.RESPONSE_FAIL, false)
	pass

	get_tree().call_group("robot_console", "add_text", "override_press_x_to_exit", Globals.ConsoleTextType.DIRECTIVE, false)


func enter_low_battery_mode():
	set_mode(Mode.LOW_BATTERY)
	Globals.console_add_text("low_battery", Globals.ConsoleTextType.RESPONSE_TIMEOUT)


func exit_low_battery_mode():
	set_mode(Mode.NORMAL)


func recharge_battery():
	pass


func set_health(amount:float):
	if amount <= 0:
		set_mode(Mode.LOW_BATTERY)
		if alive:
			sfx.play("Die")
			alive = false
			fire_laser_end()
			kernel_panic()


func set_battery(amount:float):
	if amount <= 0:
		set_mode(Mode.LOW_BATTERY)
	else:
		if Globals.Player.attributes.health > 0:
			set_mode(Mode.NORMAL)


func get_fuel_count():
	var settings = Globals.get_settings_by_difficulty()
	return Globals.Player.inventory.count(1)


func get_goal_count():
	var settings = Globals.get_settings_by_difficulty()
	return floor(float(settings.CrystalsPerCrater) * float(settings.CraterCount) * settings.CrystalsGoal)


func show_goals():
	var total_crystals_available = get_goal_count()
	var crystals_in_inventory = get_fuel_count()
	if crystals_in_inventory == 0:
		return
	Globals.console_add_text("fuel_acquired (%d/%d)" % [ crystals_in_inventory, total_crystals_available ], Globals.ConsoleTextType.RESPONSE_OK, true)


func _on_GoalsTimer_timeout():
#	show_goals()
	pass


func show_end_goal():
	Globals.console_add_text("return_to_habitat", Globals.ConsoleTextType.DIRECTIVE, true)
	pass


func check_end_goal():
	var goal_count = get_goal_count()
	var crystals_in_inventory = get_fuel_count()
#	print("crystals_in_inventory=%d goal_count=%d" % [ crystals_in_inventory, goal_count ])
	if int(crystals_in_inventory) >= int(goal_count):
		show_end_goal()

