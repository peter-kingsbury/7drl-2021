extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_CollisionArea_body_entered(body):
	print("entered")
	get_tree().call_group("game_over", "show_dialog")


func _on_CollisionArea_body_exited(body):
	print("left")
	get_tree().call_group("game_over", "hide_dialog")
