extends "res://Scenes/TemplateItem/TemplateItem.gd"

onready var mined_progress = $MiningProgressBar
onready var destruction_timer = $DestructionTimer
onready var sfx = $SFX

var damage:float = 0
var max_damage:float = 100
var damage_speed:float = 25
var is_exploding = false
var explosion = null

var Explosion = load("res://Scenes/Explosion/Explosion.tscn")
var CrystalShards = load("res://Scenes/CrystalShards/CrystalShards.tscn")

func _ready():
	pass


func _process(delta):
	mined_progress.apply_damage(damage)
	if damage >= max_damage:
		explode()


func apply_laser(delta:float):
	mined_progress.show()
	damage += delta * damage_speed
	#mined_progress.progress_bar.value = mined_progress.progress_bar.value - int(damage)


func explode():
	if explosion == null:
		add_explosion()
		replace_with_shards()


func add_explosion():
	var parent = get_parent()
	explosion = Explosion.instance()
	explosion.position = position
	parent.add_child(explosion)


func replace_with_shards():
	var parent = get_parent()
	var crystal_shards = CrystalShards.instance()
	crystal_shards.hide()
	parent.add_child(crystal_shards)
	crystal_shards.position = position
	crystal_shards.show()
	queue_free()
