extends PopupDialog

onready var toggle_fullscreen = $TabContainer/Graphics/HBoxContainer/ToggleFullscreen
onready var enable_crt = $TabContainer/Graphics/HBoxContainer2/EnableCRT


func _ready():
	pass


func recenter():
	var pos = rect_position
	var size = rect_size
	var win = get_viewport().size
	
	rect_position = win/2 - size/2


func toggle_fullscreen():
	hide()
	
	Globals.toggle_fullscreen()
	
	recenter()
	show()


func enable_scanlines(enabled:bool):
	if enabled == true:
		get_tree().call_group("crt_shader", "show")
	else:
		get_tree().call_group("crt_shader", "hide")
	Globals.Options.enable_crt_shader = enabled


func _on_ToggleFullscreen_pressed():
	toggle_fullscreen()


func _on_EnableCRT_pressed():
	enable_scanlines(enable_crt.pressed)


func _on_OKButton_pressed():
	hide()


func _on_HSlider_value_changed(value):
	if value == 0:
			Globals.set_difficulty(Globals.Difficulty.EASY)
	elif value == 1:
			Globals.set_difficulty(Globals.Difficulty.NORMAL)
	elif value == 2:
			Globals.set_difficulty(Globals.Difficulty.HARD)


func _on_MasterVolumeSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), value)


func _on_SfxVolumeSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sfx"), value)


func _on_MusicVolumeSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), value)


func _on_UIVolumeSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("UI"), value)
