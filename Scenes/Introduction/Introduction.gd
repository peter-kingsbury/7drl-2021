extends Control


onready var rich_text = $RichTextLabel
onready var press_continue_key = $RichTextLabel2
onready var timer = $Timer

export var speed:float = 0.04


func _ready():
	press_continue_key.hide()
	rich_text.percent_visible = 0


func _process(delta):
	rich_text.percent_visible += delta * speed
	if rich_text.percent_visible >= 1.0:
		press_continue_key.show()
	

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			get_tree().change_scene("res://Game.tscn")

