extends RichTextLabel

onready var timer = $Timer
onready var sfx = $SFX


func _ready():
	visible_characters = 0


func format_from_type(text:String, type:int):
	match type:
		Globals.ConsoleTextType.NORMAL:
			return text
		Globals.ConsoleTextType.DIRECTIVE:
			return '[color=#4488ff]directive("%s")[/color]' % [ text ]
		Globals.ConsoleTextType.RESPONSE_NORMAL:
			return '> [color=#ffffff]%s[/color]' % [ text ]
		Globals.ConsoleTextType.RESPONSE_DEBUG:
			return '> [color=#888888]%s[/color]' % [ text ]
		Globals.ConsoleTextType.RESPONSE_OK:
			return '> [color=#00ff00]%s[/color]' % [ text ]
		Globals.ConsoleTextType.RESPONSE_TIMEOUT:
			return '> [color=#ff8800]%s[/color]' % [ text ]
		Globals.ConsoleTextType.RESPONSE_FAIL:
			return '> [color=#ff0000]%s[/color]' % [ text ]
	pass


func init(text:String, type:int, animated = true):
	if animated:
		visible_characters = 0
	else:
		percent_visible = 1

	bbcode_text = format_from_type(text, type)

	if animated:
		timer.start()
	else:
		timer.queue_free()


func _on_Timer_timeout():
	visible_characters = visible_characters + 1
	sfx.play("Click")
	if visible_characters == get_text().length():
		timer.stop()
		timer.queue_free()

