extends Control

var ConsoleText = load("res://Scenes/RobotConsole/ConsoleText.tscn")

onready var container = $ScrollContainer
onready var vbox_container = $ScrollContainer/VBoxContainer
onready var vbox_scrollbar = $ScrollContainer.get_v_scrollbar()
onready var sfx = $SFX

export var maximum_log_lines = 0
export var add_line_after_seconds = 0.1


func _ready():
	add_text(" ", Globals.ConsoleTextType.NORMAL)
	add_text(" ", Globals.ConsoleTextType.NORMAL)
	add_text(" ", Globals.ConsoleTextType.NORMAL)
	add_text(" ", Globals.ConsoleTextType.NORMAL)
	add_text(" ", Globals.ConsoleTextType.NORMAL)
	add_text(" ", Globals.ConsoleTextType.NORMAL)
#	add_text(" ", Globals.ConsoleTextType.NORMAL)
#	add_text("ready", Globals.ConsoleTextType.RESPONSE_NORMAL)
#	debug_testing()
	pass


func debug_testing():
	add_text("do_directive", Globals.ConsoleTextType.DIRECTIVE)
	add_text("normal output", Globals.ConsoleTextType.NORMAL)
	add_text("response debug", Globals.ConsoleTextType.RESPONSE_DEBUG)
	add_text("response fail", Globals.ConsoleTextType.RESPONSE_FAIL)
	add_text("response normal", Globals.ConsoleTextType.RESPONSE_NORMAL)
	add_text("response ok", Globals.ConsoleTextType.RESPONSE_OK)
	add_text("response timeout", Globals.ConsoleTextType.RESPONSE_TIMEOUT)


func add_text(text:String, type:int, animate = true) -> void:
	# Wait a bit
	yield(get_tree().create_timer(add_line_after_seconds), "timeout")
	
	var console_text = ConsoleText.instance()
	vbox_container.add_child(console_text)
	console_text.init(text, type, animate)
	maintain_log_output()
	yield(get_tree(), "idle_frame")
	container.scroll_vertical = int(vbox_scrollbar.max_value)


func maintain_log_output() -> void:
	if maximum_log_lines <= 0:
		return
	if vbox_container.get_child_count() > maximum_log_lines:
		vbox_container.get_child(0).queue_free()


func clear_console():
	for c in vbox_container.get_children():
		vbox_container.remove_child(c)
		c.queue_free()
