extends Node2D

export (int, "None", "Overland", "Subterranean") var terrain_type = 0
export var starting_overlay_color:Color = Color.white
export var battery_recharge_rate:float = 0.0

onready var navigation = $Navigation2D
onready var portals = $Portals
onready var craters = $Craters
onready var npcs = $NPCs
onready var items = $Items
onready var players = $Player
onready var portal_timer = $PortalTimer
onready var robot_console = $GUI/Control/RobotConsole

var used_locations = []

var player
var terrain


func _ready():
	connect_portal_signals()
	player = players.find_node("Player", true, false)
	Globals.player_update_attributes()


func connect_portal_signals():
	for portal in portals.get_children():
		portal.connect("portal_body_entered", self, "on_portal_body_entered")


func on_portal_body_entered(scene_path, portal_id):
	print("Transitioning to level %s" % [ scene_path ])


func _global_tick_timeout():
	Globals.log("TemplateLevel tick timeout")


func generate_tilemap_v2(scene_file_name:String):
	var terrain = load(scene_file_name).instance()
	terrain.generate = false
	terrain.generate()
	return terrain


func save_cells(tilemap) -> Dictionary:
	var cells = {}
	for j in range(0, tilemap.map_h):
		for i in range(0, tilemap.map_w):
			cells["%d,%d" % [i, j]] = tilemap.get_cell(i, j)
	return cells


func restore_cells(tilemap, cells):
	for j in range(0, tilemap.map_h):
		for i in range(0, tilemap.map_w):
			tilemap.set_cell(i, j, cells["%d,%d" % [i, j]])
	tilemap.auto_tile()
	return tilemap


func location_is_used(loc:Vector2) -> bool:
	if used_locations.find(loc) == -1:
		return false
	return true

func use_location(loc:Vector2):
	used_locations.push_back(loc)


func _on_PortalTimer_timeout():
	get_tree().call_group("portal", "activate")


