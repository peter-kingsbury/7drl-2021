extends Node2D


onready var label = $NinePatchRect/Label

var letters = [
	"a","b","c","d","e","f","g","h","i","j","k","l","m",
	"n","o","p","q","r","s","t","u","v","w","x","y","z"
]

func _ready():
	pass


func set_text(text):
	label.text = text


func set_random_text(length = 5):
	var text = ""
	for i in range(length):
		var index = Globals.rng.randi_range(0, letters.size() - 1)
		text = text + letters[index]
	set_text(text)

func set_sleep_text():
	label.text = "h h h . . ."
