extends "res://Scenes/TemplateCharacter/TemplateCharacter.gd"

onready var speech_bubble = $SpeechBubble
onready var animated_sprite = $AnimatedSprite
onready var items_sprite = $Items
onready var behaviour_timer = $BehaviourTimer
onready var speech_timer = $SpeechTimer
onready var movement_tween = $MovementTween
onready var light = $Light2D

onready var up_arrow = $Debug/UpArrow
onready var down_arrow = $Debug/DownArrow
onready var left_arrow = $Debug/LeftArrow
onready var right_arrow = $Debug/RightArrow
onready var state_label = $Debug/StateLabel
onready var tile_position_label = $Debug/TilePositionLabel

onready var sfx = $SFX

onready var health_bar = $Control/HealthProgressBar

enum Behaviour { 
	IDLE,  		# 0
	WANDER,  	# 1
	NOTICE,		# 2
	CHASE,		# 3
	ATTACK,		# 4
	SLEEP,		# 5
	
	ALIGN		# 6
}

var current_behaviour = Behaviour.WANDER
var player_tile_position = null
var walk_toward_tile = null
var move_direction = null
var move_speed = 0.01
export var movement_time_seconds = 0.25
var target = null
var damage:float = 0
var damage_speed:float = 40

# Ready ########################################################################

func _ready():
	hide_speech_bubble()
	hide_health_bar()
	update_tile_position()
	debug_hide_arrows()
	update_state_label()
	set_default_animation()
	
	speech_bubble.set_random_text()


func hide_health_bar():
	health_bar.hide()


func show_health_bar():
	health_bar.show()


func set_default_animation():
	animated_sprite.play("default")
	pass


func update_state_label():
	state_label.text = behaviour_to_text()


func debug_hide_arrows():
	up_arrow.hide()
	down_arrow.hide()
	left_arrow.hide()
	right_arrow.hide()


func _process(delta:float):
	tile_position_label.text = "(%d, %d)" % [ tile_position.x, tile_position.y ]
	return


# Various Functions ############################################################
func set_animation(animation_name:String):
	animated_sprite.animation(animation_name)
	items_sprite.animation(animation_name)


func show_speech_bubble():
	speech_bubble.show()
	speech_timer.start()


func hide_speech_bubble():
	speech_bubble.hide()


func _on_SpeechTimer_timeout():
	speech_bubble.hide()
	transition_state(Behaviour.CHASE)

# FSM ##########################################################################

func transition_state(to_state):
	match current_behaviour:
		Behaviour.IDLE:
			on_exit_idle()
		Behaviour.WANDER:
			on_exit_wander()
		Behaviour.NOTICE:
			on_exit_notice()
		Behaviour.CHASE:
			on_exit_chase()
		Behaviour.ATTACK:
			on_exit_attack()
		Behaviour.SLEEP:
			on_exit_sleep()
		_:
			Globals.log("Unahndled source behaviour %d" % [ current_behaviour ])
	
	match to_state:
		Behaviour.IDLE:
			on_enter_idle()
		Behaviour.WANDER:
			on_enter_wander()
		Behaviour.NOTICE:
			on_enter_notice()
		Behaviour.CHASE:
			on_enter_chase()
		Behaviour.ATTACK:
			on_enter_attack()
		Behaviour.SLEEP:
			on_enter_sleep()
		_:
			Globals.log("Unahndled target behaviour %d" % [ to_state ])
	
	current_behaviour = to_state
	update_state_label()


# IDLE
func on_enter_idle():
	pass


func process_idle(delta:float):
	pass


func on_exit_idle():
	pass


# WANDER
func on_enter_wander():
	pass


func process_wander(delta:float):
	var dir = get_random_direction()
	#update_direction_arrows(dir)
	move_toward_tile(dir)
	

func on_exit_wander():
	pass


# NOTICE
func on_enter_notice():
	show_speech_bubble()
	sfx.play("Alert")


func process_notice(delta:float):
	pass


func on_exit_notice():
	pass

# CHASE
func on_enter_chase():
	pass


func process_chase(delta:float):
	var dir = calculate_move_direction()
	update_direction_arrows(dir)
	move_toward_tile(dir)


func on_exit_chase():
	pass

# ATTACK
func on_enter_attack():
	pass


func process_attack(delta:float):
	if not target:
		return
	
	var settings = Globals.get_settings_by_difficulty()
	Globals.player_apply_damage(settings.EnemyDamagePerHit)
	sfx.play_random(["Hit1","Hit2","Hit3","Hit4","Hit5"])


func on_exit_attack():
	pass


# SLEEP
func on_enter_sleep():
	sfx.play("Sleep")
	animated_sprite.play("sleep")
	speech_bubble.show()
	speech_bubble.set_sleep_text()
	$LaserHitArea/CollisionPolygon2D.disabled = true
	health_bar.hide()
	Globals.console_add_text("threat_neutralized", Globals.ConsoleTextType.RESPONSE_DEBUG)
	light.hide()


func process_sleep(delta:float):
	pass


func on_exit_sleep():
	pass


#func _on_Area2D_body_entered(body):
#	print(body)
#
#
#func _on_Area2D_body_exited(body):
#	print(body)


func move(delta:float = 1):
#	print("Behaviour: %s %d" % [ behaviour_to_text(), current_behaviour ])
	match current_behaviour:
		Behaviour.IDLE:
			process_idle(delta)
		Behaviour.WANDER:
			process_wander(delta)
		Behaviour.NOTICE:
			process_notice(delta)
		Behaviour.CHASE:
			process_chase(delta)
		Behaviour.ATTACK:
			process_attack(delta)
		Behaviour.SLEEP:
			process_sleep(delta)
	pass

func get_random_direction() -> Vector2:
	return Vector2(Globals.rng.randi_range(-1, 1), Globals.rng.randi_range(-1, 1))


func _global_tick_timeout():
	move()


func get_direction_to_player() -> Vector2:
	return (tile_position - player_tile_position).normalized()


func set_player_tile_position(tile_pos):
	player_tile_position = tile_pos


func move_toward_tile(dir):
	var obj = self
	var property = "position"
	var initial_val = position
	var final_val = position + dir * Globals.TileSizev
	var duration = movement_time_seconds
	var trans_type = Tween.TRANS_LINEAR
	var ease_type = Tween.EASE_IN_OUT
	
	movement_tween.interpolate_property(
		obj, 
		property,
		initial_val, 
		final_val, 
		duration,
		trans_type, 
		ease_type)
	movement_tween.start()
	
	sfx.play_random(["Step1", "Step2", "Step3", "Step4"])


func update_direction_arrows(dir:Vector2):
	if dir.x == -1:
		right_arrow.hide()
		left_arrow.show()
	elif dir.x == 1:
		right_arrow.show()
		left_arrow.hide()
	elif dir.x == 0:
		right_arrow.hide()
		left_arrow.hide()
	
	if dir.y == -1:
		up_arrow.show()
		down_arrow.hide()
	elif dir.y == 1:
		up_arrow.hide()
		down_arrow.show()
	elif dir.y == 0:
		up_arrow.hide()
		down_arrow.hide()
	
	pass


func calculate_move_direction() -> Vector2:
	var dir = Vector2.ZERO
	
	# Right
	if tile_position.x < player_tile_position.x:
		dir.x = 1
	# Left
	elif tile_position.x > player_tile_position.x:
		dir.x = -1
	else:
		dir.x = 0
	
	# Down
	if tile_position.y < player_tile_position.y:
		dir.y = 1
	
	# Up
	elif tile_position.y > player_tile_position.y:
		dir.y = -1
	else:
		dir.y = 0
	
	return dir


func behaviour_to_text():
	match current_behaviour:
		Behaviour.ALIGN:
			return "Align"
		Behaviour.ATTACK:
			return "Attack"
		Behaviour.CHASE:
			return "Chase"
		Behaviour.IDLE:
			return "Idle"
		Behaviour.NOTICE:
			return "Notice"
		Behaviour.SLEEP:
			return "Sleep"
		Behaviour.WANDER:
			return "Wander"


func _on_DetectArea_body_entered(body):
	if not "is_player" in body:
		return
	
	if current_behaviour == Behaviour.SLEEP:
		return
	
	match current_behaviour:
		Behaviour.IDLE:
			continue
		Behaviour.WANDER:
			transition_state(Behaviour.NOTICE)


func _on_AttackArea_body_entered(body):
	if not "is_player" in body:
		return
	
	if current_behaviour == Behaviour.SLEEP:
		return

	# Save the target
	target = body
	
	match current_behaviour:
		Behaviour.IDLE:
			continue
		Behaviour.WANDER:
			continue
		Behaviour.NOTICE:
			continue
		Behaviour.CHASE:
			transition_state(Behaviour.ATTACK)


func _on_DetectArea_body_exited(body):
	if not "is_player" in body:
		return
	
	if current_behaviour == Behaviour.SLEEP:
		return

	var settings = Globals.get_settings_by_difficulty()
	if not settings.EnemyChaseOnLostQuarry:
		transition_state(Behaviour.WANDER)
	else:
		# Today we hunt, for tomorrow ... we hunt!
		pass


func _on_AttackArea_body_exited(body):
	if not "is_player" in body:
		return
	
	if current_behaviour == Behaviour.SLEEP:
		return

	# Reset the target
	target = body
	
	# Resume the chase
	transition_state(Behaviour.CHASE)


func apply_laser(delta:float):
	match current_behaviour:
		Behaviour.IDLE:
			transition_state(Behaviour.NOTICE)
		Behaviour.WANDER:
			transition_state(Behaviour.NOTICE)
	
	damage = clamp(damage + delta * damage_speed, 0, 100)
	
	# Update health bar visibility
	if damage <= 0 or damage >= 100:
		hide_health_bar()
	else:
		show_health_bar()
	
	health_bar.apply_damage(damage)
#	print("alien %s has been hit with a 'laser', damage=%f" % [ name, damage ])
	if damage >= 100:
		transition_state(Behaviour.SLEEP)
		print("alien %s has been knocked unconscious" % [ name ])


func align_to_tile():
	var x_offset = fmod(position.x, Globals.TileSize)
	var y_offset = fmod(position.y, Globals.TileSize)
	
	if x_offset != Globals.TileSize / 2:
		position.x = tile_position.x * Globals.TileSize + Globals.TileSize / 2
	
	if y_offset != Globals.TileSize / 2:
		position.y = tile_position.y * Globals.TileSize + Globals.TileSize / 2


func _on_MovementTween_tween_completed(object, key):
	align_to_tile()
