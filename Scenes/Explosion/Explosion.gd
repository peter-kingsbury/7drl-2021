extends Node2D

onready var particles = $Particles2D
onready var timer = $Timer
onready var sfx = $SFX

var done = false

func _ready():
	sfx.play("Explosion")
	particles.emitting = true
	timer.wait_time = particles.lifetime


func _on_Timer_timeout():
	if done == true:
		queue_free()
	done = true

func _on_Explosion_finished():
	if done == true:
		queue_free()
	done = true
