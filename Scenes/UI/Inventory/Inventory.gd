extends Control

onready var container = $NinePatchRect/ScrollContainer/VBoxContainer


var item_map = [
	"res://Scenes/UI/InventoryItem/InventoryItem.tscn",
	"res://Scenes/UI/InventoryItem/CrystalShardItem.tscn"
]


func _ready():
	pass


func add_item(item_type):
	var item = load(item_map[item_type]).instance()
	container.add_child(item)


func pop_item():
	pass
