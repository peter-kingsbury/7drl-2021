extends PopupDialog

onready var console = $VBoxContainer/RobotConsole
onready var console_timer = $ConsoleTimer

export var main_menu_scene_change_delay_seconds = 5

var status_text = {
	"normal": {
		"lines": [
			{ "text": "verify_life_support", "type": Globals.ConsoleTextType.DIRECTIVE, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "directive_fail", "type": Globals.ConsoleTextType.RESPONSE_FAIL, "animate": true },
			{ "text": "insufficient_power", "type": Globals.ConsoleTextType.RESPONSE_FAIL, "animate": true },
			{ "text": "life_support_critical", "type": Globals.ConsoleTextType.RESPONSE_FAIL, "animate": true },
			{ "text": "find_alternate_fuel_source", "type": Globals.ConsoleTextType.DIRECTIVE, "animate": true },
		]
	},
	"game_over": {
		"lines": [
			{ "text": "verify_life_support", "type": Globals.ConsoleTextType.DIRECTIVE, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "directive_ok", "type": Globals.ConsoleTextType.RESPONSE_OK, "animate": true },
			{ "text": "life_support_online", "type": Globals.ConsoleTextType.RESPONSE_OK, "animate": true },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": "terminate_emergency_override", "type": Globals.ConsoleTextType.DIRECTIVE, "animate": true },
			{ "text": "directive_ok", "type": Globals.ConsoleTextType.RESPONSE_OK, "animate": true },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": "shutdown_protocol", "type": Globals.ConsoleTextType.DIRECTIVE, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "waiting...", "type": Globals.ConsoleTextType.RESPONSE_NORMAL, "animate": true },
			{ "text": "directive_ok", "type": Globals.ConsoleTextType.RESPONSE_OK, "animate": true },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": " ", "type": Globals.ConsoleTextType.NORMAL, "animate": false },
			{ "text": "Thank you for playing Rusty Robot!", "type": Globals.ConsoleTextType.NORMAL, "animate": true },
		]
	}
}

var mode = "normal"
var index = 0
var is_active = false


func _ready():
	console.clear_console()


func set_mode_game_over():
	mode = "game_over"


func reset_index():
	index = 0


func show_dialog():
	reset_index()
	console.clear_console()
	show()
	is_active = true
	check_status()


func hide_dialog():
	is_active = false
	hide()


func check_status():
	console_timer.start()


func increment_show_line():
	if not status_text.has(mode):
		return
	
	var lines = status_text[mode].lines
	
	if lines.size() > index:
		var line = lines[index]
		console.add_text(line.text, line.type, line.animate)
		index = index + 1
	else:
		# we are at the end of the lines
		if mode == "game_over":
			yield(get_tree().create_timer(main_menu_scene_change_delay_seconds), "timeout")
			get_tree().change_scene("res://Game.tscn")


func _on_ConsoleTimer_timeout():
	increment_show_line()


func _process(delta:float):
	recenter()


func recenter():
	var pos = rect_position
	var size = rect_size
	var win = get_viewport().size
	
	rect_position = win/2 - size/2

