extends ConfirmationDialog


func _on_ExitDialog_confirmed():
	get_tree().change_scene("res://Game.tscn")


func toggle_visibility():
	visible = !visible
