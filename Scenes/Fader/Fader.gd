extends CanvasModulate

onready var animation_player = $AnimationPlayer


func _ready():
	pass



func fade_out():
	Globals.log("fade out started")
	animation_player.play("Fade Out")
	yield(animation_player, "animation_finished")
	Globals.log("fade out finished")


func fade_in():
	Globals.log("fade in started")
	animation_player.play("Fade In")
	yield(animation_player, "animation_finished")
	Globals.log("fade in finished")


func blank():
	Globals.log("blanking screen")
	color = Color.black


func _on_AnimationPlayer_animation_finished(anim_name):
	pass # Replace with function body.
