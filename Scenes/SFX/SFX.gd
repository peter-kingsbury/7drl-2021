extends Node


func _ready():
	pass


func play(name):
	var sound = find_node(name)
	if sound:
		sound.play()
	else:
		print("sfx %s not found" % [name])


func play_random_sequence(sfx:Array, delay:float = 0.1):
	for i in range(0, sfx.size()):
		play(sfx[i])
		if delay > 0:
			yield(get_tree().create_timer(delay), "timeout")


func play_random(sfx:Array):
	var rnd = RandomNumberGenerator.new()
	rnd.randomize()
	if sfx.size() == 0:
		print("sfx no sounds to play")
		return
	var i = rnd.randi_range(0, sfx.size() - 1)
	play(sfx[i])


func stop(name):
	var sound = find_node(name)
	if sound:
		sound.stop()
	else:
		print("sfx %s not found" % [name])
