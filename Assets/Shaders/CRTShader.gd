extends TextureRect


func _ready():
	if Globals.Options.enable_crt_shader == true:
		show()
	else:
		hide()
	pass


func resize():
	get_material().set_shader_param("screen_width", get_viewport().size.x)
	get_material().set_shader_param("screen_height", get_viewport().size.y)
